﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Size Characteristic", menuName = "Characteristic/Size Characteristic", order = 1)]
public class SizeCharacteristic : ScriptableObject
{
    public Size size;
    public int scaleFactor;
    public PointAttribute pointAttribute;
}

