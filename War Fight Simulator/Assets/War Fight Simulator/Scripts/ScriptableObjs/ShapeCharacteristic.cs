﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Shape Characteristic", menuName = "Characteristic/Shape Characteristic", order = 0)]
public class ShapeCharacteristic : ScriptableObject
{
    public ShapeAttribute shapeAttribute;
}

