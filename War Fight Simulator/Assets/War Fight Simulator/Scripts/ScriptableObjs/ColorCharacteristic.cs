﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Color Characteristic", menuName = "Characteristic/Color Characteristic", order = 2)]
public class ColorCharacteristic : ScriptableObject
{
    public Color color = Color.white;
    public List<ShapeAttribute> shapeAttributtes;
}