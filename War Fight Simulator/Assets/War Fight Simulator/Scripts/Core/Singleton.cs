﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    protected static T instance;


    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
                if (instance == null)
                    Debug.LogError($"Instance of type {typeof(T)} not found. Make sure it's on the scene.");

            }
            return instance;
        }
    }
}
