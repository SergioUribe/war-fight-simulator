﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Iterator : MonoBehaviour
{
    [SerializeField] GameObject iterableParent;
    List<Iterable> iterables;

    public bool IsFirstPosition => index == 0;
    public bool IsLastPosition => index == iterables.Count - 1;

    int index = 0;

    private void Awake()
    {
        CheckIterableParent();
        SetIterables();
    }

    private void CheckIterableParent()
    {
        if (iterableParent == null)
            iterableParent = gameObject;
    }

    private void SetIterables() => iterables = new List<Iterable>(iterableParent.GetComponentsInChildren<Iterable>(true));

    protected virtual void OnEnable()
    {
        index = 0;
        iterables.ForEach(g => g.gameObject.SetActive(false));
        SetCurrentIterableActive(true);
    }

    public void NextIterable() => GoToIterable(1);
    public void PreviousIterable() => GoToIterable(-1);

    protected virtual void GoToIterable(int amount)
    {
        int desiredIndex = index + amount;
        if (desiredIndex >= iterables.Count || desiredIndex < 0) return;
        SetCurrentIterableActive(false);
        index = desiredIndex;
        SetCurrentIterableActive(true);
    }

    private void SetCurrentIterableActive(bool active)
    {
        iterables[index].gameObject.SetActive(active);
    }


}
