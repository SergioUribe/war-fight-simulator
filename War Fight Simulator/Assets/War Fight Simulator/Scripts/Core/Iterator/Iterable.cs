﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Iterable : MonoBehaviour
{
    [SerializeField] UnityEvent onEnable;
    private void OnEnable() => onEnable?.Invoke();
}
