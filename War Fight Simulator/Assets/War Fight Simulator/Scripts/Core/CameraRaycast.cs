﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycast : MonoBehaviour
{

    public Vector3? GetPosition<T>(T instance) where T : MonoBehaviour
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, 1000f)) return null;
        var component = hit.collider.GetComponent<T>();
        if (component == null || component != instance) return null;
        return hit.point;
    }


}

