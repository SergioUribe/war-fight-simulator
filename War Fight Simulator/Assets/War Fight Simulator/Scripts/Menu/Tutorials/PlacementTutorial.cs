﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlacementTutorial : MonoBehaviour
{
    public Animator CurrentArmyTutorialAnim => UnitCustomizer.Instance.CurrentUnitArmy?.GetComponentInChildren<Animator>();

    private void Start() => UnitCustomizer.OnArmyFull += PlayCurrentArmyTutorial;

    private void OnDestroy() => UnitCustomizer.OnArmyFull -= PlayCurrentArmyTutorial;

    public void PlayCurrentArmyTutorial() => CurrentArmyTutorialAnim?.Play("Tutorial Add Unit");
}
