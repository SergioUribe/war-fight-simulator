﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Button))]
public class BtnSize : MonoBehaviour
{
    public Size size;

    public void Start() => GetComponent<Button>().onClick.AddListener(delegate { UnitCustomizerDisplay.Instance.CurrentUnitModel.Size = size; });
}

