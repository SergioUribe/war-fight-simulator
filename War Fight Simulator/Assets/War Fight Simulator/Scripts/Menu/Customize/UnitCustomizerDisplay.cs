﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitCustomizerDisplay : Singleton<UnitCustomizerDisplay>
{
    [SerializeField] GameObject unitModelsGroup;

    public UnitModel CurrentUnitModel { get; set; }

    private void OnEnable() => unitModelsGroup.SetActive(true);
    private void OnDisable() => unitModelsGroup.SetActive(false);


    public void SetUnitColor(string color) => CurrentUnitModel.Color = color;

    public void SetUnitSize(string size) => CurrentUnitModel.Size = EnumParser.Parse<Size>(size);


}

public static class EnumParser
{
    public static T Parse<T>(string value) where T : Enum => (T)Enum.Parse(typeof(T), value);
}

