﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ReadyToPlayChecker : MonoBehaviour
{
    [SerializeField] GameObject btnBack, btnPlay;

    private void Update()
    {
        btnBack.SetActive(!UnitCustomizer.Instance.ReadyToPlay);
        btnPlay.SetActive(UnitCustomizer.Instance.ReadyToPlay);
    }
}


