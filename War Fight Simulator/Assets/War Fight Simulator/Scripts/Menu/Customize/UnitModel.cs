﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class UnitModel : MonoBehaviour
{
    [SerializeField] Unit unitPrefab;
    [SerializeField] string defaultColor = "Blue";
    private Size size = Size.SMALL;
    private string color;

    public Size Size
    {
        get => size; set
        {
            size = value;
            UpdateScale();
        }
    }

    public string Color
    {
        get => color; set
        {
            color = value;
            UpdateColor();
        }
    }

    private void OnEnable()
    {
        if (string.IsNullOrEmpty(Color))
            Color = defaultColor;
        UpdateData();
        UnitCustomizerDisplay.Instance.CurrentUnitModel = this;
    }

    private void UpdateData()
    {
        if (UnitCustomizerDisplay.Instance.CurrentUnitModel == null) return;
        Color = UnitCustomizerDisplay.Instance.CurrentUnitModel.Color;
        Size = UnitCustomizerDisplay.Instance.CurrentUnitModel.Size;
    }


    public Material Material { get; private set; }

    private void Awake()
    {
        Material = GetComponent<Renderer>().material;

    }

    private void UpdateScale() => transform.localScale = Vector3.one * (Size == Size.BIG ? 2 : 1);
    private void UpdateColor() => Material.color = AttributesUnit.Instance.GetColor(Color);

    public UnitData GetUnitData()
    {
        return new UnitData
        {
            shape = unitPrefab,
            size = Size,
            color = Color
        };
    }
}

