﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttributesUnit : Singleton<AttributesUnit>
{
    #region Serialized Variables
    [Header("Unit Attributes")]
    [SerializeField] PointAttribute basicUnitAttribute = new PointAttribute { hp = 100, atk = 10 };
    [SerializeField] List<ShapeCharacteristic> shapeCharacteristics;
    [SerializeField] List<SizeCharacteristic> sizeCharacteristics;
    [SerializeField] List<ColorCharacteristic> colorCharacteristics;

    [Header("Unit Point Rates")]
    [SerializeField] PointSpeed hpSpeedRate = new PointSpeed(150, 10, 450, 5);
    [SerializeField] PointSpeed atkSpeedRate = new PointSpeed(30, 1, 70, 2);

    [Header("Unit Army Values")]
    [SerializeField] int maxUnitsPerArmy = 20;
    #endregion


    #region Properties
    public PointSpeed HpSpeedRate => hpSpeedRate;
    public PointSpeed AtkSpeedRate => atkSpeedRate;
    public int MaxUnitsPerArmy => maxUnitsPerArmy;

    #endregion

    public UnitCharacteristic GetUnitChar(UnitData unitData)
    {
        return new UnitCharacteristic
        {
            basicUnitAttribute = basicUnitAttribute,
            shapeChar = shapeCharacteristics.Find(sh => sh.shapeAttribute.shape == unitData.shape),
            sizeChar = sizeCharacteristics.Find(si => si.size == unitData.size),
            colorChar = colorCharacteristics.Find(c => c.name.Equals(unitData.color))
        };
    }

    public Color GetColor(string colorName) => colorCharacteristics.Find(c => c.name.Equals(colorName)).color;

}

[System.Serializable]
public class PointSpeed
{
    [SerializeField] int minPointsValue, minPointsSpeedValue;
    [SerializeField] int maxPointsValue, maxPointsSpeedValue;

    public PointSpeed(int minPointsValue, int minPointsSpeedValue, int maxPointsValue, int maxPointsSpeedValue)
    {
        this.minPointsValue = minPointsValue;
        this.minPointsSpeedValue = minPointsSpeedValue;
        this.maxPointsValue = maxPointsValue;
        this.maxPointsSpeedValue = maxPointsSpeedValue;
    }

    public float GetSpeed(int maxUnitPts)
    {
        var pointFactor = GetPointFactor(maxUnitPts);
        var speedDifference = maxPointsSpeedValue - minPointsSpeedValue;
        var speed = pointFactor * speedDifference + minPointsSpeedValue;
        return speed;
    }

    private float GetPointFactor(int maxUnitPts)
    {
        var pointDifference = maxPointsValue - minPointsValue;
        var pointFactor = (float)(maxUnitPts - minPointsValue) / pointDifference;
        return Mathf.Clamp(pointFactor, 0, 1);
    }
}
