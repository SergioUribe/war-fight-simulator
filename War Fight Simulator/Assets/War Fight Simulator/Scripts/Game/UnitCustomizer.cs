﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CameraRaycast))]
public class UnitCustomizer : Singleton<UnitCustomizer>
{
    [SerializeField] MeshRenderer aim;
    public UnitCustomizerDisplay Display { get; private set; }

    public CameraRaycast CameraRaycast { get; private set; }
    public UnitArmy CurrentUnitArmy { get; private set; }

    public static event Action OnArmyFull = null;

    public bool IsCustomizingUnit => Display.gameObject.activeSelf;

    public bool ReadyToPlay { get; private set; } = false;

    int armyIndex = 0;

    private void Awake() => SetComponentProperties();

    private void SetComponentProperties()
    {
        CameraRaycast = GetComponent<CameraRaycast>();
        Display = GetComponentInChildren<UnitCustomizerDisplay>();
    }

    private void Start() => UpdateCurrentUnitArmy();

    void Update() => CheckMouseInput();

    private void CheckMouseInput()
    {
        CheckMousePosition();
        CheckLeftMouseClick();
    }

    private void CheckMousePosition()
    {
        Vector3? pos = CameraRaycast.GetPosition(CurrentUnitArmy);
        aim.enabled = pos != null && !IsCustomizingUnit;
        if (pos == null) return;
        var aimPos = new Vector3(pos.Value.x, aim.transform.position.y, pos.Value.z);
        aim.transform.position = aimPos;
    }

    private void CheckLeftMouseClick()
    {
        if (IsCustomizingUnit || !Input.GetMouseButtonDown(0) || ReadyToPlay) return;
        Vector3? pos = CameraRaycast.GetPosition(CurrentUnitArmy);
        if (pos == null || !aim.enabled) return;
        var unitPos = pos.Value;
        unitPos.y = 0;
        CurrentUnitArmy.CreateUnit(Display.CurrentUnitModel.GetUnitData(), GetSnappedToIntVector(pos.Value));
        CheckArmyIsFull();
    }

    Vector3 GetSnappedToIntVector(Vector3 vector) => new Vector3((int)vector.x, (int)vector.y, (int)vector.z);

    private void CheckArmyIsFull()
    {
        if (!CurrentUnitArmy.Full) return;
        armyIndex++;
        UpdateCurrentUnitArmy();
        OnArmyFull?.Invoke();
    }

    private void UpdateCurrentUnitArmy()
    {
        CurrentUnitArmy = UnitArmiesHandler.Instance.AllArmiesFull ? null : UnitArmiesHandler.Instance.UnitArmies[armyIndex];
        CheckReadyToPlay();
    }

    private void CheckReadyToPlay() => ReadyToPlay = CurrentUnitArmy == null && UnitArmiesHandler.Instance.AllArmiesFull;
}



