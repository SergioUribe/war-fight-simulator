﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Mover))]
[RequireComponent(typeof(Attacker))]
[RequireComponent(typeof(Health))]
public class Unit : MonoBehaviour
{

    public float Apothem => transform.GetChild(0).localScale.x / 2;

    #region Unit Components

    public Material Material { get; private set; }
    public Mover Mover { get; private set; }
    public Attacker Attacker { get; private set; }
    public Health Health { get; private set; }

    #endregion

    #region Unit State Methods



    #endregion

    protected virtual void Awake() => SetComponentPropierties();

    private void SetComponentPropierties()
    {
        Material = GetComponentInChildren<Renderer>().material;
        Mover = GetComponent<Mover>();
        Attacker = GetComponent<Attacker>();
        Health = GetComponent<Health>();
    }

    #region Unit Creation Methods

    public void Create(UnitCharacteristic unitChar)
    {
        var pivot = transform.GetChild(0);
        pivot.localScale *= unitChar.sizeChar.scaleFactor;
        Mover.Agent.radius *= unitChar.sizeChar.scaleFactor;
        Material.color = unitChar.colorChar.color;
        SetAllAttributePoints(unitChar);
        InitializeUnitPoints();
        UpdateSpeedValues();
    }

    private void UpdateSpeedValues()
    {
        Attacker.MinDistanceToAttack += Apothem;
    }

    private void InitializeUnitPoints() => Health.RestoreHealth();

    private void SetAllAttributePoints(UnitCharacteristic unitChar)
    {
        AddAttributePoint(unitChar.basicUnitAttribute);
        AddAttributePoint(unitChar.shapeChar.shapeAttribute.pointAttribute);
        AddAttributePoint(unitChar.sizeChar.pointAttribute);
        var colorPointAttribute = unitChar.colorChar.shapeAttributtes.Find(sh => sh.shape.GetType() == GetType());
        AddAttributePoint(colorPointAttribute.pointAttribute);
    }

    public void AddAttributePoint(PointAttribute attribute)
    {
        Health.MaxHP += attribute.hp;
        Attacker.ATK += attribute.atk;
    }
    #endregion
}

public class UnitCharacteristic
{
    public PointAttribute basicUnitAttribute;
    public ShapeCharacteristic shapeChar;
    public SizeCharacteristic sizeChar;
    public ColorCharacteristic colorChar;

}
