﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class UnitArmiesHandler : Singleton<UnitArmiesHandler>
{
    private UnitArmy winner;

    public UnitArmy Winner
    {
        get => winner; set
        {
            winner = value;
            GameController.Instance?.StopGame();
        }
    }

    public bool AllArmiesFull => UnitArmies.FindAll(a => a.Full).Count == UnitArmies.Count;

    public List<UnitArmy> UnitArmies { get; private set; }

    private void Awake() => UnitArmies = new List<UnitArmy>(GetComponentsInChildren<UnitArmy>());


    public UnitArmy GetEnemyArmyCreator(UnitArmy unitCreator) => UnitArmies.Find(u => u != unitCreator);
}
