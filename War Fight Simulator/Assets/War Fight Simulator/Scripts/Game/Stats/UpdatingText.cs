﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Voodoo.ArmyClash.Stats
{

    [RequireComponent(typeof(Text))]
    public abstract class UpdatingText : MonoBehaviour
    {
        protected Text Text { get; private set; }

        private void Awake() => Text = GetComponent<Text>();

        protected virtual void Update() => UpdateText();
        protected abstract void UpdateText();
    }

}

