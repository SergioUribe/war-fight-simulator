﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Voodoo.ArmyClash.Stats
{

    public class WinnerDisplay : UpdatingText
    {
        protected override void UpdateText()
        {
            var winnerText = UnitArmiesHandler.Instance.Winner.TextName;
            Text.color = winnerText.color;
            Text.text = $"{winnerText.text} wins!";
        }


    }

}