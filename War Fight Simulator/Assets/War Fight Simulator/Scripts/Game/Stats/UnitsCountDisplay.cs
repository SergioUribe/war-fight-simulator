﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Voodoo.ArmyClash.Stats
{

    public class UnitsCountDisplay : UpdatingText
    {
        [SerializeField] UnitArmy unitArmy;

        protected override void UpdateText() => Text.text = $"{unitArmy.Units.Count} / {AttributesUnit.Instance.MaxUnitsPerArmy}";
    }

}
