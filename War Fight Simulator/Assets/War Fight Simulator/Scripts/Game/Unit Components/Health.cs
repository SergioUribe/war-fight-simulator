﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int MaxHP { get; set; }

    public Action OnDeath { get; set; }

    private int hp;

    public int HP
    {
        get => hp; private set
        {
            hp = Mathf.Clamp(value, 0, MaxHP);
            if (Dead)
                OnDeath?.Invoke();
        }
    }
    public bool Dead => HP == 0;

    public void TakeDamage(int value) => HP -= value;

    public void RestoreHealth() => HP = MaxHP;

    private void Start() => OnDeath += Die;

    private void Die() => gameObject.SetActive(false);

}

