﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Mover))]
public class Attacker : MonoBehaviour
{
    [SerializeField] float minDistanceToAttack = 0.3f;

    public float MinDistanceToAttack { get; set; }

    public int ATK { get; set; }
    public float AttackSpeed => AttributesUnit.Instance.AtkSpeedRate.GetSpeed(ATK);
    private float timeSinceLastAttack = Mathf.Infinity;
    private Unit target;

    public UnitArmy UnitArmy { get; private set; }


    public Unit Target
    {
        get => target; private set
        {
            target = value;
            MinDistanceToAttack += Target == null ? 0 : Target.Apothem;
        }
    }

    public bool CanAttack => Vector3.Distance(transform.position, Target.transform.position) < MinDistanceToAttack;

    public Mover Mover { get; set; }

    private void Awake() => SetComponentProperties();

    private void SetComponentProperties()
    {
        Mover = GetComponent<Mover>();
        UnitArmy = GetComponentInParent<UnitArmy>();
    }

    private void Start()
    {
        MinDistanceToAttack += minDistanceToAttack;
    }

    private void Update()
    {
        if (!GameController.Instance.IsPlaying) return;
        CheckCanAttack();
    }

    private void CheckCanAttack()
    {
        if (!HasTarget() || UnitArmy.EnemyArmy.AllUnitsDead)
        {
            Mover.Agent.ResetPath();
            return;
        }
        if (!CanAttack)
            Mover.MoveTo(Target.transform.position);
        else
            Attack();
    }

    private bool HasTarget()
    {
        if (Target != null) return true;
        Target = UnitArmy.GetNearestEnemy(transform);
        return Target != null;
    }

    private void Attack()
    {
        Mover.Stop();
        timeSinceLastAttack += Time.deltaTime;
        if (IsTargetDead() || timeSinceLastAttack < AttackSpeed) return;
        timeSinceLastAttack = 0f;
        Target?.Health.TakeDamage(ATK);

    }

    public bool IsTargetDead()
    {
        if (!Target.Health.Dead) return false;
        MinDistanceToAttack -= Target.Apothem;
        timeSinceLastAttack = Mathf.Infinity;
        Target = null;
        return true;
    }

}
