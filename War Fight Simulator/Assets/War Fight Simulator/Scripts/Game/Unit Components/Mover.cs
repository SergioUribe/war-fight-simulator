﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Health))]
public class Mover : MonoBehaviour
{
    public NavMeshAgent Agent { get; private set; }
    public Health Health { get; private set; }

    public float MoveSpeed => AttributesUnit.Instance.HpSpeedRate.GetSpeed(Health.HP);

    private void Awake() => SetComponentPropierties();

    private void SetComponentPropierties()
    {
        Agent = GetComponent<NavMeshAgent>();
        Health = GetComponent<Health>();
    }

    private void Update() => Agent.speed = MoveSpeed;

    public bool IsCloser(Transform target, Transform compared) => Vector3.Distance(target.position, transform.position) < Vector3.Distance(target.position, compared.position);

    public void MoveTo(Vector3 destination)
    {
        Agent.isStopped = false;
        Agent.SetDestination(destination);
    }

    public void Stop() => Agent.isStopped = true;
}


