﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class GameController : Singleton<GameController>
{
    [SerializeField] UnityEvent onGameFinished;
    private bool finished = false;

    public bool IsPlaying { get; private set; } = false;
    public bool Finished
    {
        get => finished; private set
        {
            finished = value;
            if (finished)
                onGameFinished?.Invoke();
        }
    }
    public void StartGame() => IsPlaying = true;

    internal void StopGame()
    {
        IsPlaying = false;
        Finished = true;
    }
}

