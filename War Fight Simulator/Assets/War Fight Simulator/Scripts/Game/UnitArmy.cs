﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitArmy : MonoBehaviour
{
    [SerializeField] List<UnitData> units;
    [SerializeField] Text armyName;

    public List<Unit> Units { get; private set; } = new List<Unit>();
    public UnitArmy EnemyArmy { get; private set; }

    public bool Full => Units.Count == AttributesUnit.Instance.MaxUnitsPerArmy;
    public bool AllUnitsDead => Units.Count == 0;
    public Text TextName => armyName;

    float offset = -5.5f * 3;

    private void Start()
    {
        EnemyArmy = UnitArmiesHandler.Instance?.GetEnemyArmyCreator(this);
        SpawnUnits();
    }

    public void SpawnUnits()
    {
        units.ForEach(CreateUnit);
    }

    private void Update()
    {
        CheckUnitsCount();
    }

    private void CheckUnitsCount()
    {
        if (!GameController.Instance.IsPlaying || !AllUnitsDead) return;
        UnitArmiesHandler.Instance.Winner = EnemyArmy;
    }

    private void CreateUnit(UnitData unitData)
    {
        Vector3 pos = Vector3.zero;
        pos.z = transform.position.z;
        pos.x = offset;
        CreateUnit(unitData, pos);
        offset += 4;
    }

    public void CreateUnit(UnitData unitData, Vector3 startingPos)
    {
        var unitChar = AttributesUnit.Instance?.GetUnitChar(unitData);
        var unit = Instantiate(unitChar.shapeChar.shapeAttribute.shape, startingPos, Quaternion.identity, transform);
        unit.Create(unitChar);
        unit.Health.OnDeath += delegate { Units.Remove(unit); };
        Units.Add(unit);
    }

    public Unit GetNearestEnemy(Transform transform)
    {
        if (EnemyArmy.AllUnitsDead) return null;
        var nearestEnemyIndex = 0;
        for (int i = 0; i < EnemyArmy.Units.Count; i++)
        {
            if (EnemyArmy.Units[i].Mover.IsCloser(transform, EnemyArmy.Units[nearestEnemyIndex].transform))
                nearestEnemyIndex = i;
        }
        return EnemyArmy.Units[nearestEnemyIndex];
    }

}

[System.Serializable]
public class UnitData
{
    public Unit shape;
    public Size size;
    public string color;

}


