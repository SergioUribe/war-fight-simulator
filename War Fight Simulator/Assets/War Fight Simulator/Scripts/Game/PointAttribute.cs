﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Size
{
    BIG,
    SMALL
}

[System.Serializable]
public class PointAttribute
{
    public int hp;
    public int atk;
}

[System.Serializable]
public class ShapeAttribute
{
    public Unit shape;
    public PointAttribute pointAttribute;
}


