﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : Singleton<SceneLoader>
{
    [SerializeField] GameObject panelLoading;
    [SerializeField] Animator fadeAnim;

    public int CurrentSceneIndex => SceneManager.GetActiveScene().buildIndex;

    public string CurrentSceneName => SceneManager.GetActiveScene().name;

    public void LoadFirstScene() => LoadScene(0);

    public void LoadNextScene() => LoadScene(CurrentSceneIndex + 1);

    public void LoadPreviousScene() => LoadScene(CurrentSceneIndex - 1);

    public void ReloadScene() => LoadScene(CurrentSceneIndex);

    public void LoadScene(int sceneIndex) => StartCoroutine(CallLoadScene(sceneIndex));

    public void LoadScene(string sceneName) => StartCoroutine(CallLoadScene(sceneName));

    #region Coroutines
    IEnumerator CallLoadScene(string sceneName)
    {
        if (fadeAnim != null)
            yield return FadeOut();
        if (panelLoading == null)
            SceneManager.LoadScene(sceneName);
        else
            yield return LoadingProcess(sceneName);

    }

    IEnumerator CallLoadScene(int sceneIndex)
    {
        var scenePath = SceneUtility.GetScenePathByBuildIndex(sceneIndex);
        yield return CallLoadScene(GetSceneNameByPath(scenePath));
    }


    IEnumerator LoadingProcess(string sceneName)
    {
        AsyncOperation progress = SceneManager.LoadSceneAsync(sceneName);
        if (!progress.isDone)
        {
            panelLoading.SetActive(true);
            yield return null;
        }
    }

    IEnumerator FadeOut()
    {
        fadeAnim.SetTrigger("fadeOut");
        yield return new WaitForEndOfFrame();
        float length = fadeAnim.GetCurrentAnimatorStateInfo(0).length;
        yield return new WaitForSeconds(length);
    }
    #endregion

    #region Static Methods
    public static string GetSceneNameByPath(string path)
    {
        var pathRoutes = path.Split('/');
        var sceneNameFormat = pathRoutes[pathRoutes.Length - 1];
        var format = ".unity";
        return sceneNameFormat.Remove(sceneNameFormat.Length - format.Length);
    }
    #endregion

    public void Quit() => Application.Quit();

}
